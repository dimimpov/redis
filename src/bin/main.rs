// use redis::read;
use redis::MAP;
// use redis::WRITE;

use redis::ThreadPool;
use serde_json::json;
use std::fs;
use std::io::prelude::*;
use std::io::BufReader;
use std::net::TcpListener;
use std::net::TcpStream;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    let pool = ThreadPool::new(4);

    for stream in listener.incoming() {
        let stream = stream.unwrap();

        pool.execute(|| {
            handle_connection(stream);
        });
    }
}

fn handle_connection(mut stream: TcpStream) {
    // let mut buffer = [0; 512];
    let mut buf = String::new();

    // stream.read(&mut buffer).unwrap();
    let mut reader = BufReader::new(&stream);
    reader.read_line(&mut buf).expect("could not read");
    // let substring: String = buf.to_string()[7..].to_string();
    // let args = &substring.replace("HTTP/1.1", "");
    // let key_value: Vec<&str> = args.split('=').collect();
    // println!("{} : {}", key_value[0], key_value[1]);

    let post = "POST";
    let get = "GET / HTTP/1.1\r\n";

    // println!("num {:?}.", VAR.lock().unwrap());
    // let mut response: String = "".to_string();
    let (status_line, response) = if buf.starts_with(post) {
        // response = set(buf);
        ("HTTP/1.1 200 OK\r\n\r\n", set(buf))
    } else {
        // response = get_variable();
        ("HTTP/1.1 200 OK\r\n\r\n", get_variable(buf))
    };

    // let contents = fs::read_to_string(filename).unwrap();

    let final_response = format!("{}{}", status_line, response);

    stream.write(final_response.as_bytes()).unwrap();
    stream.flush().unwrap();
}

fn set(buf: String) -> String {
    let substring: String = buf.to_string()[7..].to_string();
    let args = &substring.replace("HTTP/1.1", "");
    let key_value: Vec<&str> = args.split('=').collect();
    let mut w = MAP.1.lock().unwrap();
    w.insert(key_value[0].to_string(), key_value[1].to_string());
    w.refresh();
    return "Stored".to_string();
}

fn get_variable(buf: String) -> String {
    // let reviews = MAP.0.lock().unwrap();
    let substring: String = buf.to_string()[7..].to_string();
    let args = &substring.replace("HTTP/1.1", "");
    let key_value: Vec<&str> = args.split('=').collect();

    let key = key_value[1].trim().to_string();
    // let len = key.len();
    // key.truncate(len - 1);

    let mut res: String = "".to_string();
    if let Some(reviews) = MAP.0.lock().unwrap().get(&key) {
        for review in &*reviews {
            res = review.trim().to_string();
        }
    } else {
        res = "Not found".to_string();
    }
    let response = json!({ key: res });
    return response.to_string();
}
