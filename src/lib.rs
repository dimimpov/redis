#[macro_use]
use lazy_static::lazy_static;
use std::collections::hash_map::RandomState;
use std::fmt;
use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread; // 1.4.0

// let (map_r, mut map_w) = evmap::new();

pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Job>,
}

// let map_w = evmap::new().1;

// #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
// pub struct MyHash(pub [str; 32]);

// impl fmt::Display for MyHash {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(f, "{:X?}", self.0)
//     }
// }

// impl evmap::ShallowCopy for MyHash {
//     #[inline]
//     unsafe fn shallow_copy(&mut self) -> Self {
//         *self
//     }
// }
// lazy_static! {
//     // pub static ref map_r:  = evmap::new().0;
//     pub static ref WRITE: Arc<Mutex<evmap::WriteHandle<String,String>>> =  Arc::new(Mutex::new(evmap::new().1));

//     pub static ref READ: Arc<Mutex<evmap::ReadHandle<String, String>>> = Arc::new(Mutex::new(evmap::new().0));
// }

type ReadHandle = evmap::ReadHandle<String, String, (), RandomState>;
type WriteHandle = evmap::WriteHandle<String, String, (), RandomState>;

lazy_static! {
    pub static ref MAP: (Arc<Mutex<ReadHandle>>, Arc<Mutex<WriteHandle>>) = {
        let (r, w) = evmap::new();
        (Arc::new(Mutex::new(r)), Arc::new(Mutex::new(w)))
    };
}
pub struct Variable {
    pub var: String,
}

impl Variable {
    pub fn show(self) -> String {
        return self.var;
    }
}

type Job = Box<dyn FnOnce() + Send + 'static>;

impl ThreadPool {
    /// Create a new ThreadPool.
    ///
    /// The size is the number of threads in the pool.
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the size is zero.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();

        let receiver = Arc::new(Mutex::new(receiver));

        // let counter = Arc::new(Mutex::new(Variable {
        //     var: "Hello".to_string(),
        // }));

        // let write_To_Memory = WRITE.clone();

        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            let write_To_Memory = Arc::clone(&MAP.1);

            workers.push(Worker::new(id, Arc::clone(&receiver), write_To_Memory));
        }

        ThreadPool { workers, sender }
    }
    // pub fn spawn<F, T>(f: F) -> JoinHandle<T>
    // where
    //     F: FnOnce() -> T + Send + 'static,
    //     T: Send + 'static,
    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender.send(job).unwrap();
    }
}
struct Worker {
    id: usize,
    thread: thread::JoinHandle<()>,
    // counter: Arc<Mutex<i32>>,
}

impl Worker {
    fn new(
        id: usize,
        receiver: Arc<Mutex<mpsc::Receiver<Job>>>,
        write_To_Memory: Arc<Mutex<evmap::WriteHandle<String, String>>>,
    ) -> Worker {
        let thread = thread::spawn(move || loop {
            let job = receiver.lock().unwrap().recv().unwrap();
            // let mut w = write_To_Memory.lock().unwrap();
            // w.refresh();

            // let num = counter.lock().unwrap();
            // a
            // println!("num {}.", *VAR.lock().unwrap());

            println!("Worker {} got a job; executing.", id);
            job();
        });
        // println!("Result: {}", *counter.lock().unwrap());

        Worker {
            id,
            thread,
            // counter,
        }
    }
}
