#! /bin/bash


set -e

chmod +x updateAndRestart.sh

# ssh ${USERNAME}@${PUBLIC_IP} "bash" < ./updateAndRestart.sh
sshpass -p "$PASSWORD" ssh -o StrictHostKeyChecking=no ${USERNAME}@${PUBLIC_IP} "bash" < ./updateAndRestart.sh
done